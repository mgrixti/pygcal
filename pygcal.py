from datetime import datetime
import os
import time
from apiclient.discovery import build
from httplib2 import Http
import oauth2client
from oauth2client import client
from oauth2client import tools
from caltime import get_timeMin, get_timeMax, getTZ
import eventMan
import sys
import argparse
import re

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'gcal'

# main: Controls what is called bases on flags/args 
def main():
    r = re.compile('^\d{4}-\d{2}-\d{2}$')
    
    # Determins if to pass a date
    if args.date == "":
        getCal()
    elif r.match(str(args.date)) is not None:
        getCal(args.date)

# getCal 
# Takes a string argument (yyyy-dd-mm)
# Gets the calendar and prints it
def getCal(d=""):

    # if no date get cal from current time on. Else get cal for date given
    if d == "":
        minTime = getTZ(datetime.now()).isoformat()
        maxTime = get_timeMax(datetime.today().strftime("%Y-%m-%d"))
    else:
            minTime = get_timeMin(d)    
            maxTime = get_timeMax(d)

    print '\n', '=============================================================================', '\n'
    print ("         " + getTZ(datetime.now()).strftime("%c")+'\n' )
    
    # Get users oauth2 credentials
    credentials = get_credentials()
    service = build('calendar', 'v3', http=credentials.authorize(Http()))
   
    # Gets calendar from goole 
    eventsResult = service.events().list(
        calendarId='primary', timeMin=minTime, timeMax=maxTime, singleEvents=True, orderBy='startTime').execute()
    events = eventsResult.get('items', [])

    
    if not events:
        print 'No upcoming events found.'
    i=1
    for event in events: # Prints events
        time = event['start'].get('dateTime')
        print ("%d." %i), event['summary'].ljust(30) + time[11:-9]
        i += 1
    
    # if "-m" flag was specified call menu
    if args.m:
        eventMan.mainMenu(events, service)

# Gets users_credentials from google/get user to authorize for the first time
# Writen by google
def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-quickstart.json')
    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run(flow, store)
        print 'Storing credentials to ' + credential_path
    return credentials

# Calls main and sets args/flags
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("date", type=str, nargs='?', default="", help="yyyy-mm-dd for the day you want to view")
    parser.add_argument("-m", help="show menu for events", action="store_true")
    args = parser.parse_args()
    
    main() 
