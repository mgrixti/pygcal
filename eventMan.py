from datetime import datetime, timedelta
import caltime

#The main menu collects input after todays events are displayed
#Redirects the user to a new menu (or ends program) based on input
#Checks that input is valid
def mainMenu(events, service):
    print '----- <#>View Event Details <c>Create An Event <q>Quit -----'
    sele = raw_input('Your choice => ')
   
 
    try:
    	selection = int(sele)
    	maxValue = len(events)
                
        if 0 < selection <=  maxValue:
            print('option 1')
    	    getMore(selection, events, service)
        else:
            print('option 2')
            mainMenu(events, service) 

    except ValueError:

    	if sele is 'c':
            print('option 3')
            createEvent(service)

    	elif sele is not 'q':
            print('option 4')
    	    mainMenu(events, service)
        else:
            print 'Quitting...'

#This is the menu displayed after event details have been displayed
#If user edits, calls customizeEvent (edit events details) and then calls itself when the function is done
#If user quits, takes them back to the main menu
#If user deletes, deletes event and brings user to mainMenu
#On wrong input recalls itself
def eventMenu(events, service, event):
    print '----- <e>Edit <d>Delete <q>Quit -----'
    sele = raw_input('Your choice => ')
    if sele == 'e':
        customizeEvent(service, event)
        eventMenu(events, service, event)
    elif sele == 'q':
        mainMenu(events, service)
    elif sele == 'd':
        deleteEvent(service, event)
        mainMenu(events, service)
    else:
        eventMenu(events, service, event)

#Displays event Details   	
def getMore(number,events, service):

    print '\n', '=============================================================================', '\n'

    number = number - 1
    eId = events[number][u'id']
    eveObj = service.events().get(calendarId='primary',eventId=eId).execute()

    summary = events[number]['summary']
    print summary.upper()+':'

    eveStatus = eveObj.get('status')
    print 'Event Status: ', eveStatus

    updatedTime = eveObj.get('updated')
    print 'Last Updated: ', updatedTime

    location = eveObj.get('location')
    print 'Event Location: ', location

    visability = eveObj.get('visibility')
    print 'Event Visability: ', visability

    description = eveObj.get('description')
    print 'Event Description: ', description

    attendees = eveObj.get('attendees')
    if not attendees:
        print 'Guests: No attendees'
    else:
        print 'Guests:'
        for attendee in attendees:
            name = attendee.get('displayName')
            if not name:
                name = attendee.get(u'email')
            status = attendee['responseStatus']
            print name.ljust(40), status
    eventMenu(events, service, eveObj)

#Deletes selected event
def deleteEvent(service, event):
    delEvent = input('Are you sure you want to delete this event?(y,N):')

    if delEvent == 'y' or 'Y':
        service.events().delete(calanderId='primary', eventId=event['id'], sendNotifications=true)
        print('Event deleted.')
    else:
        print('Event NOT  deleted.')

#Creates a new event (default options initially)
#Calls customizeEvent (to allow setting event details)
def createEvent(service):
    print '\n', '=============================================================================', '\n'
    print 'CREATE AN EVENT \n'
    eventName = raw_input("Event Name: ")
    newEvent = service.events().quickAdd(calendarId='primary',text= eventName ).execute()
    newEvent['summary']=eventName
    updateEvent = service.events().update(calendarId='primary', eventId=newEvent['id'], body=newEvent, sendNotifications=True).execute()
    customizeEvent(service, newEvent)

#Allows users to edit event details
#Calls function that performs action based on selection, then updates the event
#Goes to event menu on quit
def customizeEvent(service, event):
    print '\n 1. Set Event Time'
    print ' 2. Change Event Status'
    print ' 3. Add Location'
    print ' 4. Add Description'
    print ' 5. Edit Visability'
    print ' 6. Invite Guest'

    print('----- <#>Edit Field <q>Quit -----')
    selection = raw_input('Your choice =>  ')
    if selection == 'q':
        eventMenu(service, event)
  
    try:
        selection = int(selection)
        selection = selection - 1
        eventEditMenu = [eventTime, eventStatus, eventLocation, eventDescription, eventVisability, eventGuest]
        event = eventEditMenu[selection](event, service)
        updateEvent = service.events().patch(calendarId='primary', eventId=event['id'], body=event, sendNotifications=True).execute()
        customizeEvent(service, event)
 
    except ValueError:
        customizeEvent(service, event)

#Gets input from user to change event time, changes eventTime        
def eventTime(event, service):
    print ('\n EVENT TIME:')
    year = input('Year: ')
    month = input('Month (1-12): ')
    day = input('Day: ')
    hour = input ('Hour: ')
    minute = input('Minute: ')
    length = input('Length of event(hours): ')

    startTime = caltime.getTZ(datetime(year, month, day, hour, minute, 0, 0)).isoformat()
    event['start']['dateTime'] = startTime
    endTime = caltime.getTZ(datetime(year, month, day, hour, minute, 0, 0) + timedelta(hours=length)).isoformat()
    event['end']['dateTime'] = endTime
    return event

#Gets input from user to change eventStatus, changes eventStatus
def eventStatus(event,service):
    print '\n EVENT STATUS: '
    print ' 1. Confirmed \n 2. Tentative \n 3. Cancelled'
    choice = input('Your choice =>  ')
    if choice == 1:
        event['status'] = 'confirmed'
    elif choice == 2:
        event['status'] = 'tentative'
    elif choice == 3:
        event['status'] = 'cancelled'
    return event

#Gets input from user to change eventLocation, changes eventLocation
def eventLocation(event,service):
    print('\n EVENT LOCATION: ')
    eLocation = raw_input('Your choice => ')
    event['location'] = eLocation
    return event

#Gets input from user to change eventDescription, changes eventDiscription
def eventDescription(event, service):
    eDescription = raw_input('Event Description: ')
    event['description']= eDescription
    return event

#Gets input from user to change eventVisability, changes eventVisability
def eventVisability(event, service):
    print '\n EVENT VISABILITY: '
    print ' 1. Default-Uses the default visability for the events on the calendar \n 2. Public-The event is public and the event details are visable to all readers of the calendar \n 3. Private-The event is private and only event attendees may view event details \n 4. Confidential-The event is private. This value is provided for compatibility reasons'
    choice = input('Your choice =>  ')
    if choice == 1:
        event['visability'] = 'default'
    elif choice == 2:
        event['visability'] = 'public'
    elif choice == 3:
        event['visability'] = 'private'
    elif choice == 4:
        event['visability'] = 'confidential'
    return event

#Gets input from user for attendees email, adds attendee with email field
def eventGuest(event, service):
    print('\n INVITE GUEST: ')
    guestEmail = raw_input('Enter your guest\'s email: ')

    attendees = event.get('attendees', [])
    attendees.append({"email": guestEmail})
    event['attendees'] = attendees
    updateEvent = service.events().patch(calendarId='primary', eventId=event['id'], body=event, sendNotifications=True).execute()
    return event

