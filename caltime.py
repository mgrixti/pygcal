# Created by: Matthew Grixti & Kendra Wannamaker
# Handles time internations for the calendar app 
from datetime import datetime
import os
import time
import re

APPLICATION_NAME = 'gcal'

# Gets timezone from user if they have not set it and localizes time to that timezone
def getTZ(time):
    from pytz import timezone
    
    # Creates dir if it doesnt exist
    home_dir = os.path.expanduser('~')
    tz_dir = os.path.join(home_dir, '.caltz')
    if not os.path.exists(tz_dir):
        os.makedirs(tz_dir)
    
    usertz = 'none'
    if not os.path.exists(tz_dir + '/tz'):
        tzfile = open(tz_dir + '/tz', 'w')
        usertz = raw_input('Enter your timezone (eg. MST):')
        tzfile.write(usertz)
        tzfile.close()
    else:
        tzfile = open(home_dir + '/.caltz/tz', 'r')
        usertz = tzfile.readline()
        tzfile.close()
    # Localizes date to user time zone
    time = timezone(usertz).localize(time)
    return time

# Gets start of day time in users timezone for date passed.
def get_timeMin(date):
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date [8:])

    date = datetime(year, month, day, 0, 0, 0, 000000)
    return getTZ(date).isoformat()

# Gets end of date time in users time zone for date passed.
def get_timeMax(date):
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date [8:])

    date = datetime(year, month, day, 23, 59, 59, 999999)
    return getTZ(date).isoformat()
